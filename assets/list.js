function filterRegion() {
    let dropdown = document.getElementById('region-filter');
    let value = dropdown.options[dropdown.selectedIndex].value;

    urlParts = window.location.href.split('?');
    urlParts[1] = value === "" ? "" : "?region="+value;

    window.location.href = urlParts.join("");
}