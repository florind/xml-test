<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Countries</title>
    <link rel="stylesheet" type="text/css" href="../assets/list.css">
</head>
<body>
    <div class="table">
        <div class="header">
            <div class="row">
                <div class="cell">
                    Regiune
                    <select name="region" id="region-filter" onchange="filterRegion()">
                        <option value="" <?php echo isset($region_filter) ?: 'selected' ?>>choose</option>
                        <option value="western" <?php echo isset($region_filter) && $region_filter === "western" ? 'selected' : ''?>>western</option>
                        <option value="central" <?php echo isset($region_filter) && $region_filter === "central" ? 'selected' : ''?>>central</option>
                        <option value="eastern" <?php echo isset($region_filter) && $region_filter === "eastern" ? 'selected' : ''?>>eastern</option>
                    </select>
                </div>
                <div class="cell">Țară</div>
                <div class="cell">Limbă</div>
                <div class="cell">Monedă</div>
                <div class="cell">Latitudine</div>
                <div class="cell">Longitudine</div>
            </div>
        </div>
        <div class="body">
            <?php foreach($countries as $country) { ?>
                <div class="row">
                    <div class="cell"><?php echo htmlspecialchars($country->getZone()) ?></div>
                    <div class="cell"><?php echo htmlspecialchars($country->getName()) ?> (<?php echo htmlspecialchars($country->getNativeName()) ?>)</div>
                    <div class="cell"><?php echo htmlspecialchars($country->getLanguage()) ?> (<?php echo htmlspecialchars($country->getNativeLanguage()) ?>)</div>
                    <div class="cell"><?php echo htmlspecialchars($country->getCurrency()) ?> (<?php echo htmlspecialchars($country->getCurrencyCode()) ?>)</div>
                    <div class="cell"><?php echo htmlspecialchars($country->getLatitude()) ?></div>
                    <div class="cell"><?php echo htmlspecialchars($country->getLongitude()) ?></div>
                </div>
            <?php } ?>
        </div>
    </div>

    <p>Countries that use the Euro currency: <?php echo implode(', ', $euro_countries) ?></p>
    <script src="../assets/list.js"></script>
</body>
</html>