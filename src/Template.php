<?php

/**
 * Class Template
 *
 * Minimal template render helper
 */
class Template
{
    /**
     * @var string $path
     */
    private static $path = __ROOT__. '/view/';

    /**
     * @param $view
     * @param array $parameters
     */
    public static function render(string $view, array $parameters)
    {
        ob_start();
        extract($parameters);
        include static::$path . $view;
        echo ob_get_clean();

        exit;
    }
}