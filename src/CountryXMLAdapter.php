<?php

/**
 * Class CountryXMLAdapter
 *
 * Adapter for SimpleXMLElement country node
 */
class CountryXMLAdapter
{
    /** @var string $zone */
    private $zone;

    /** @var string $name */
    private $name;

    /** @var string $nativeName */
    private $nativeName;

    /** @var string $language */
    private $language;

    /** @var string $nativeLanguage */
    private $nativeLanguage;

    /** @var string $currency */
    private $currency;

    /** @var string $currencyCode */
    private $currencyCode;

    /** @var string $longitude */
    private $longitude;

    /** @var string $latitude */
    private $latitude;

    /**
     * CountryXMLAdapter constructor.
     *
     * @param SimpleXMLElement $element
     */
    public function __construct(SimpleXMLElement $element)
    {
        $this->zone = (string) $element->attributes()->zone;
        $this->name = (string) $element->name;
        $this->nativeName = (string) $element->name->attributes()->native;
        $this->language = (string) $element->language;
        $this->nativeLanguage = (string) $element->language->attributes()->native;
        $this->currency = (string) $element->currency;
        $this->currencyCode = (string) $element->currency->attributes()->code;

        preg_match("/\/@(?'lat'-*\d+.\d+),(?'long'-*\d+.\d+)/", $element->map_url, $position);
        $position = array_filter($position, "is_string", ARRAY_FILTER_USE_KEY);

        $this->latitude = $position['lat'] ?? "";
        $this->longitude = $position['long'] ?? "";
    }

    /**
     * @return string
     */
    public function getZone(): string
    {
        return $this->zone;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getNativeName(): string
    {
        return $this->nativeName;
    }

    /**
     * @return string
     */
    public function getLanguage(): string
    {
        return $this->language;
    }

    /**
     * @return string
     */
    public function getNativeLanguage(): string
    {
        return $this->nativeLanguage;
    }

    /**
     * @return string
     */
    public function getCurrency(): string
    {
        return $this->currency;
    }

    /**
     * @return string
     */
    public function getCurrencyCode(): string
    {
        return $this->currencyCode;
    }

    /**
     * @return string
     */
    public function getLatitude(): string
    {
        return $this->latitude;
    }

    /**
     * @return string
     */
    public function getLongitude(): string
    {
        return $this->longitude;
    }
}