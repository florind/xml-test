<?php

/**
 * Class CountryXMLRepository
 *
 * Repository on top of countries XML
 */
class CountryXMLRepository
{
    /** @var SimpleXMLIterator $iterator */
    private $iterator;

    /**
     * @param string $filename
     *
     * @return CountryXMLRepository
     */
    public static function fromFilePath(string $filename): self
    {
        $iterator = @simplexml_load_file($filename, SimpleXMLIterator::class);

        if (false === $iterator) {
            throw new RuntimeException(sprintf('Failed to load XML file "%s".', $filename));
        }

        return new self($iterator);
    }

    /**
     * CountryXMLRepository constructor.
     *
     * @param SimpleXMLIterator $iterator
     */
    public function __construct(SimpleXMLIterator $iterator)
    {
        $this->iterator = $iterator;
    }

    /**
     * @return array
     */
    public function fetchAll(): array
    {
        $list = [];

        for ($this->iterator->rewind(); $this->iterator->valid(); $this->iterator->next()) {
            $list[] = new CountryXMLAdapter($this->iterator->current());
        }

        return $this->sort($list);
    }

    /**
     * @param array $parameters
     *
     * @return array
     */
    public function where(array $parameters): array
    {
        $query = $this->prepareQuery($parameters);
        $results = $this->iterator->xpath($query);

        $countries = [];

        foreach ($results as $result) {
            $countries[] = new CountryXMLAdapter($result);
        }

        return $this->sort($countries);
    }

    /**
     * @param array $parameters
     *
     * @return string
     */
    private function prepareQuery(array $parameters): string
    {
        $query = "./country{{zone}}{{params}}";

        if (array_key_exists('zone', $parameters)) {
            $q = sprintf("[@zone='%s']", $parameters['zone']);
            $query = str_replace("{{zone}}", $q, $query);
            unset($parameters['zone']);
        } else {
            $query = str_replace("{{zone}}", "", $query);
            unset($parameters['zone']);
        }

        if (empty($parameters)) {
            return str_replace("{{params}}", "", $query);
        }

        $parts = [];

        foreach ($parameters as $param => $value) {
            $parts[] = sprintf("./%s='%s'", $param, $value);
        }

        $subquery = sprintf("[%s]", implode(" and ", $parts));
        $query = str_replace("{{params}}", $subquery, $query);

        return $query;
    }

    /**
     * @param array $countries
     *
     * @return array
     */
    private function sort(array $countries): array
    {
        usort($countries, function (CountryXMLAdapter $a, CountryXMLAdapter $b) {
            $order = strcmp($a->getZone(), $b->getZone());
            return 0 === $order ? strcmp($a->getName(), $b->getName()) : $order;
        });

        return $countries;
    }
}