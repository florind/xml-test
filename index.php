<?php
define('__ROOT__', dirname(__FILE__));
require_once(__ROOT__ . '/src/CountryXMLAdapter.php');
require_once(__ROOT__ . '/src/CountryXMLRepository.php');
require_once(__ROOT__ . '/src/Template.php');

$xmlPath = "xml/countries.xml";

try {
    $repository = CountryXMLRepository::fromFilePath($xmlPath);
} catch (RuntimeException $exception) {
    echo $exception->getMessage();
    exit;
}

$euroCountryNames = array_map(function(CountryXMLAdapter $country) {
    return $country->getName();
}, $repository->where(['currency' => 'Euro']));

if (array_key_exists("region", $_GET)) {
    $countries = $repository->where(['zone' => $_GET['region']]);

    Template::render('list.php', [
        'countries' => $countries,
        'region_filter' => $_GET['region'],
        'euro_countries' => $euroCountryNames
    ]);
} else {
    Template::render('list.php', [
        'countries' => $repository->fetchAll(),
        'euro_countries' => $euroCountryNames
    ]);
}
